package main

import (
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"bytes"
	"fmt"
	"time"
	"math/rand"
	"strconv"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"encoding/json"
)

var error_struct map[string]interface{}

func initParam()map[string]string{
	return map[string]string{
		"apiKey": "xnd_production_OIiIfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV2gg==", //production
		//"apiKey": "xnd_development_P4yAfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV3hg==",
		"url": "https://api.xendit.co/",
	}
}

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func main() {
	http.HandleFunc("/disbursment", disbursment)
	http.ListenAndServe(":7031", nil)
}

func disbursment(w http.ResponseWriter, r *http.Request) {

	param := initParam()
	client := &http.Client{}

	body, _ := ioutil.ReadAll(r.Body)

	bankCode,_ := jsonparser.GetString(body, "bank_code")
	accountName,_ := jsonparser.GetString(body, "account_holder_name")
	accountNumber,_ := jsonparser.GetString(body, "account_number")
	t := time.Now()
	date := t.Format("2006-01-02 15:04:05")
	description := "Cashout Digiro.in Tanggal "+ date
	amount,_ := jsonparser.GetInt(body, "amount")

	randId := randInt(0000000000,9999999999)
	externalId := bankCode+strconv.Itoa(randId)

	var jsonprep string = `{"external_id":"`+externalId+`","bank_code":"`+bankCode+`","account_holder_name":"`+accountName+`",`+
		`"account_number":"`+accountNumber+`","description":"`+description+`","amount":`+ strconv.Itoa(int(amount))+`}`

	var jsonStr = []byte(jsonprep)
	req, err := http.NewRequest("POST", param["url"]+"disbursements", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-IDEMPOTENCY-KEY", externalId)
	req.SetBasicAuth(param["apiKey"], "")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Unable to reach the server.")
	}else{
		error_code := ""
		code := 201
		messages := "SUCCESS"
		bodyRes, _ := ioutil.ReadAll(resp.Body)
		error_code,_ = jsonparser.GetString(bodyRes, "error_code")

		if error_code != ""{
			if err := json.Unmarshal([]byte(string(bodyRes)), &error_struct); err != nil {
				panic(err)
			}
			messages = error_struct["message"].(string)
			code = 401
		}
		w.WriteHeader(code)
		println(string(bodyRes))
		respMsg := `{"error_code":"`+strconv.Itoa(code)+`","message":"`+messages+`",}`
		w.Write([]byte(respMsg))
	}
}

func randInt(min int, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}
